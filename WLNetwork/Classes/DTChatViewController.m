//
//  DTChatViewController.m
//  RACDemo
//
//  Created by wealon on 2018/2/5.
//  Copyright © 2018年 DT. All rights reserved.
//

#import "DTChatViewController.h"

@interface DTChatViewController ()

@end

@implementation DTChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)chatWith:(NSString *)name msg:(NSString *)msg
{
    NSLog(@"chat with:%@ with msg: %@", name, msg);
    NSLog(@"---");
    NSLog(@"***");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
