//
//  main.m
//  WLNetwork
//
//  Created by wealon on 02/05/2018.
//  Copyright (c) 2018 wealon. All rights reserved.
//

@import UIKit;
#import "WLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WLAppDelegate class]));
    }
}
