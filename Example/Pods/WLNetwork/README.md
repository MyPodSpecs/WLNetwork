# WLNetwork

[![CI Status](http://img.shields.io/travis/wealon/WLNetwork.svg?style=flat)](https://travis-ci.org/wealon/WLNetwork)
[![Version](https://img.shields.io/cocoapods/v/WLNetwork.svg?style=flat)](http://cocoapods.org/pods/WLNetwork)
[![License](https://img.shields.io/cocoapods/l/WLNetwork.svg?style=flat)](http://cocoapods.org/pods/WLNetwork)
[![Platform](https://img.shields.io/cocoapods/p/WLNetwork.svg?style=flat)](http://cocoapods.org/pods/WLNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WLNetwork is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WLNetwork'
```

## Author

wealon, linxianliuzhixiang@163.com

## License

WLNetwork is available under the MIT license. See the LICENSE file for more info.
