//
//  DTChatViewController.h
//  RACDemo
//
//  Created by wealon on 2018/2/5.
//  Copyright © 2018年 DT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTChatViewController : UIViewController

- (void)chatWith:(NSString *)name msg:(NSString *)msg;

@end
